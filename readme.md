### Integration script for client: Districomp

## Introduction

This script was created to run in the generic integration platform that links Sagal's clients' various forms of sending data into payloads that the business platform can understand. Districomp is a client that mainly sends their data through a SOAP API, exposing endpoints that Sagal can request from.

## Workflow

- Article data is retrieved by performing either a full or partial request to the client's SOAP API.
- Following a request to another endpoint for additional article information, the data is restructured and processed for each article, being transformed to a Sagal-friendly format.
- The restructured data is dispatched to Sagal.

## Environment Variables

- **CLIENT_ID**: Sagal's known identifier for the client. A number.
- **CLIENT_INTEGRATION_ID**: the integration script's id in the existing runtime environment. A number.
- **CLIENT_ECOMMERCE**: a map holding this client's known identifiers for each E-Commerce service. (e.g: Key MELI for Mercadolibre)
- **HTTP_USER**: the username needed to access the client's SOAP API.
- **HTTP_PASS**: the password needed to access the client's SOAP API.
- **MAX_ARTICLES_TO_BUFFER**: the maximum number of articles that should be processed concurrently.
- **REQUEST_TYPE**: Whether the request to be done is partial or full.

## Functions

### retrieveFullArticlesFromSoapApi()

**Description:** retrieves Districomp's article data from the SOAP API endpoint for a full request, and returns a json object containing all retrieved articles.

### retrievePartialArticlesFromSoapApi()

**Description:** retrieves Districomp's article data from the SOAP API endpoint for a partial request, and returns a json object containing all retrieved articles.

### processAllArticles(Object articles)

**Description:** processes every article by calling the function processArticle. Articles are processed concurrently by being placed into an asynchronous buffer according to the max buffer size.

### processArticle(Object article)

**Description:** Processes the article by restructuring its data and dispatching it to Sagal. Also retrieves the article's image data and descriptions by calling a function to retrieve them from an API endpoint.

### getFullArticleInfo(String articleSku)

**Description:** retrieves an article's image data and descriptions by requesting them from Districomp's API endpoint.

### getArticlePrice(Object article)

**Description:** assembles the price object containing all pricing data for the article by processing the article's pricing attributes.
