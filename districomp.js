import xml2js from "https://esm.sh/xml2js";
import axiod from "https://deno.land/x/axiod@0.23.1/mod.ts";
import { decode } from "https://esm.sh/html-entities";

const clientId = Deno.env.get("CLIENT_ID");
const clientIntegrationId = Deno.env.get("CLIENT_INTEGRATION_ID");
const clientEcommerce = JSON.parse(Deno.env.get("CLIENT_ECOMMERCE"));
const user = Deno.env.get("HTTP_USER");
const pass = Deno.env.get("HTTP_PASS");
const maxArticleBuffer = Deno.env.get("MAX_ARTICLES_TO_BUFFER");
const requestType = Deno.env.get("REQUEST_TYPE");


const currencyMap = {
  "1": "$",
  "2": "U$S"
}

async function init() {
  let articles = {};
  if (requestType.toLowerCase() == "full") {
    articles = await retrieveFullArticlesFromSoapApi();
    await processAllArticles(articles);
  } else if (requestType.toLowerCase() == "partial") {
    articles = await retrievePartialArticlesFromSoapApi();
    await processAllArticles(articles);
  } else {
    console.log(`Invalid request type: ${requestType}`);
  }
}

//Start
await init()


async function retrieveFullArticlesFromSoapApi() {
  let response = await axiod.post(
    `http://nodum.districomp.com:8080/soap/Nodum_Prod/services/forms/v1.3/WSArticulosTodo?vista=DATOS`,
    `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dat="http://nodum.districomp.com:8080/soap/Nodum_Prod/schemas/forms/v1.3/WSArticulosTodo/DATOS">
      <soapenv:Header/>
      <soapenv:Body>
         <dat:procesarAlta>
            <!--1 or more repetitions:-->
            <dat:WSArticulosTodo>
               <!--Optional:-->
               <dat:General>
                  <!--Optional:-->
                  <dat:Cab>
                     <!--Optional:-->
                     <dat:CodigoEmpresa>01</dat:CodigoEmpresa>
                  </dat:Cab>
               </dat:General>
            </dat:WSArticulosTodo>
         </dat:procesarAlta>
      </soapenv:Body>
   </soapenv:Envelope>`,
    {
      headers: {
        Authorization: "Basic " + btoa(user + ":" + pass),
        "Content-Type": "text/xml;charset=UTF-8",
      },
      timeout: 500000,
    }
  );
  response = response.data.replaceAll("&lt;", "<");
  response = response.replaceAll("&gt;", ">");
  let parser = new xml2js.Parser();
  let parsedResponse = await parser.parseStringPromise(response);
  parsedResponse = parsedResponse["soap:Envelope"]["soap:Body"][0]["procesarAltaReturn"][0]["Resultado"][0]
  if (parsedResponse["R_WSArticulosTodo"]) {
    return parsedResponse["R_WSArticulosTodo"][0]["R_General"][0]["R_Articulos"]
  }
  return []
}

async function retrievePartialArticlesFromSoapApi() {
  let response = await axiod.post(
    `http://nodum.districomp.com:8080/soap/Nodum_Prod/services/forms/v1.3/WSArticulos?vista=DATOS`,
    `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dat="http://nodum.districomp.com:8080/soap/Nodum_Prod/schemas/forms/v1.3/WSArticulos/DATOS">
    <soapenv:Header/>
    <soapenv:Body>
       <dat:procesarAlta>
          <!--1 or more repetitions:-->
          <dat:WSArticulos>
             <!--Optional:-->
             <dat:General>
                <!--Optional:-->
                <dat:Cab>
                   <!--Optional:-->
                   <dat:CodigoEmpresa>01</dat:CodigoEmpresa>
                </dat:Cab>
             </dat:General>
          </dat:WSArticulos>
       </dat:procesarAlta>
    </soapenv:Body>
 </soapenv:Envelope>`,
    {
      headers: {
        Authorization: "Basic " + btoa(user + ":" + pass),
        "Content-Type": "text/xml;charset=UTF-8",
      },
      timeout: 500000,
    }
  );
  response = response.data.replaceAll("&lt;", "<");
  response = response.replaceAll("&gt;", ">");
  let parser = new xml2js.Parser();
  let parsedResponse = await parser.parseStringPromise(response);
  parsedResponse = parsedResponse["soap:Envelope"]["soap:Body"][0]["procesarAltaReturn"][0]["Resultado"][0]
  if (parsedResponse["R_WSArticulos"]) {
    return parsedResponse["R_WSArticulos"][0]["R_General"][0]["R_Articulos"]
  }
  return []
}
async function processAllArticles(articles) {
  let buffer = [];
  for (let article of articles) {
    if (buffer.length < maxArticleBuffer) {
      buffer.push(article);
    }
    if (buffer.length == maxArticleBuffer) {
      let batchArticleProcessing = async (articleInBuffer) => {
        await processArticle(articleInBuffer);
      };
      await Promise.all(buffer.map(batchArticleProcessing));
      buffer = [];
    }
  }
  if (buffer.length) {
    let batchArticleProcessing = async (articleInBuffer) => {
      await processArticle(articleInBuffer);
    };
    await Promise.all(buffer.map(batchArticleProcessing));
  }
}

async function processArticle(article) {
  try {
    if (article.R_cod_articulo2.toString() != "0") {
      let articleFullInfo = await getFullArticleInfo(article.R_CodigoDeArticulo.toString());

      let articleSku = article.R_cod_articulo2.toString();
      let articleName = article.R_NombreArticulo.toString();
      let articleStock = article.R_Cantidad.toString()
        ? Number(article.R_Cantidad.toString())
        : 0;

      let articleImages = []
      let articleDescription = null

      if (articleFullInfo) {
        articleImages = getArticleImages(articleFullInfo);
        articleDescription = getArticleDescription(articleFullInfo);
      }

      let articleAttributes = {
        MARCA: article.R_NombreLineaComercial.toString(),
      };

      if (!article.R_Precio.toString()) {
        articleStock = 0.0;
        console.log(
          `Article ${articleSku} did not sync because it does not have price`
        );
      }
      if (!article.R_PorcentajeImpuesto.toString()) {
        articleStock = 0.0;
        console.log(
          `Article ${articleSku} did not sync because it does not have a tax`
        );
      }
      if (!article.R_CodigoMoneda.toString()) {
        articleStock = 0.0;
        console.log(
          `Article ${articleSku} did not sync because it does not have a currency`
        );
      }

      let articlePrice = getArticlePrice(article);

      let processedArticle = {
        sku: articleSku.trim(),
        client_id: clientId,
        options:{
          merge: false
        },
        integration_id: clientIntegrationId,
        ecommerce: Object.values(clientEcommerce).map(ecommerce_id => {
          let ecommerceProps = {
            ecommerce_id: ecommerce_id,
            properties: [
              { name: articleName },
              {
                price: articlePrice,
              },
              {
                stock: articleStock,
              },
              {
                images: articleImages,
              },
              {
                attributes: articleAttributes,
              },
            ],
            variants: [],
          }
          if (articleDescription && articleDescription != "-") {
            ecommerceProps.properties.push({ description: articleDescription })
          }
          return ecommerceProps
        })
      };

      await sagalDispatch(processedArticle);
    } else {
      console.log(
        `Article is not valid: ${article.R_cod_articulo2.toString()}`
      );
    }
  } catch (e) {
    console.log(
      `Unable to process article: ${article.R_cod_articulo2.toString()}, ${e.message
      }`
    );
  }
}

function getArticleImages(articleFullInfo) {
  let articleImages = [...articleFullInfo.product.images];
  return articleImages;
}

function getArticleDescription(articleFullInfo) {
  let articleDescription = articleFullInfo.product?.spanish_description.toString() ?? " ";
  for (let description of articleFullInfo.product?.descripciones) {
    articleDescription += "\n";
    for (let [descriptionTag, descriptionBody] of Object.entries(description)) {
      articleDescription += `${descriptionTag}\n${decode(descriptionBody)}\n`;
    }
  }
  return articleDescription;
}

async function getFullArticleInfo(articleSku) {
  try {
    let response = await fetch(
      `https://districomp.com.uy/products/get-product?token=123456789&SKU=${articleSku.trim()}`
    );
    let results = await response.json();
    if (results.status == "error") {
      return null;
    }
    return results;
  } catch (e) {
    throw Error(`No se pudo obtener info para el sku: ${sku}, ${e.message}`);
  }
}

function getArticlePrice(article) {
  let price = article.R_Precio.toString()
    ? Number(article.R_Precio.toString())
    : 0.0;
  let taxPercentage = article.R_PorcentajeImpuesto.toString()
    ? Number(article.R_PorcentajeImpuesto.toString())
    : 0.0;
  let discountPercentage = article.R_Descuentodelarticulo.toString()
    ? Number(article.R_Descuentodelarticulo.toString())
    : 0.0;


  let priceWithDiscountAndTax =
    discountPercentage > 0.0
      ? Math.round((price - (price * discountPercentage) / 100) * (taxPercentage / 100 + 1) * 100) / 100
      : null;
  let priceWithTax = Math.round(price * (taxPercentage / 100 + 1) * 100) / 100;

  let currency = currencyMap[`${article.R_CodigoMoneda.toString()}`] ?? '$'

  return {
    value: priceWithTax,
    currency: currency,
    ...(discountPercentage && { offer_price: priceWithDiscountAndTax }),
  };
}
